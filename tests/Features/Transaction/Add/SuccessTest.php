<?php


namespace Features\Transaction\Add;


use App\Models\Profile;
use App\Models\Transaction;

class SuccessTest extends Common
{
    /** @var Profile */
    protected $profile;

    public function setUp(): void
    {
        parent::setUp();

        $this->initProfile();
    }

    public function testSuccess(): void
    {
        $value = '100.50';
        $description = "Пополнение на сумму $value рублей";

        $result = $this->post(route($this->routeName), [
            'user_id' => $this->profile->user_id,
            'value' => $value,
            'description' => $description,
            'type' => Transaction::TYPE_REFILL
        ]);

        $result->response->assertOk();

        $uid = data_get($result->response->decodeResponseJson(), 'data.uid');

        $this->seeInDatabase('transactions', [
            'uid' => $uid,
            'user_id' => $this->profile->user_id,
            'type' => Transaction::TYPE_REFILL,
            'value' => floatval($value)
        ]);

        $description = "Списание на сумму $value рублей";

        $result = $this->post(route($this->routeName), [
            'user_id' => $this->profile->user_id,
            'value' => $value,
            'description' => $description,
            'type' => Transaction::TYPE_DEBIT
        ]);

        $result->response->assertOk();

        $uid = data_get($result->response->decodeResponseJson(), 'data.uid');

        $this->seeInDatabase('transactions', [
            'uid' => $uid,
            'user_id' => $this->profile->user_id,
            'type' => Transaction::TYPE_DEBIT,
            'value' => -1 * floatval($value),
        ]);
    }

    protected function initProfile(): void
    {
        $this->profile = Profile::factory()->create();
    }
}
