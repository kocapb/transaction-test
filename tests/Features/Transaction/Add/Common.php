<?php


namespace Features\Transaction\Add;

use TestCase;

abstract class Common extends TestCase
{
    /** @var string */
    protected $routeName = 'transaction.add';
}
