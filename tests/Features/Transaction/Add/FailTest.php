<?php


namespace Features\Transaction\Add;

use App\Models\Profile;
use App\Models\Transaction;
use Symfony\Component\HttpFoundation\Response;

class FailTest extends SuccessTest
{
    public function testSuccess(): void
    {
        $value = '100.5';
        $description = "Списание на сумму $value рублей";

        $result = $this->post(route($this->routeName), [
            'user_id' => $this->profile->user_id,
            'value' => $value,
            'description' => $description,
            'type' => Transaction::TYPE_DEBIT
        ]);

        $result->seeStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);
        $result->seeInDatabase('profiles', [
            'user_id' => $this->profile->user_id,
            'balance' => $this->profile->balance
        ]);
    }

    protected function initProfile(): void
    {
        $this->profile = Profile::factory()->create([
            'balance' => 0
        ]);
    }
}
