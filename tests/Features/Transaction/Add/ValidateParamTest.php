<?php


namespace Features\Transaction\Add;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class ValidateParamTest
 * @package Features\Transaction\Add
 */
class ValidateParamTest extends Common
{
    public function testSuccess(): void
    {
        $this->post(route($this->routeName))
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJson()->response->assertJsonStructure([
                'user_id',
                'value',
                'type',
                'description'
            ]);

        $this->post(route($this->routeName), [
            'user_id' => 'string',
            'value' => 'string',
            'type' => 'string',
            'description' => 'string'
        ])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJson()->response->assertJsonStructure([
                'user_id',
                'value',
                'type'
            ]);
    }
}
