<?php


namespace Features\Transaction\Cancel;

use App\Models\Transaction;
use Symfony\Component\HttpFoundation\Response;

class FailTest extends Common
{
    /** @var Transaction  */
    protected $transaction;

    public function setUp(): void
    {
        parent::setUp();

        $this->transaction = Transaction::factory()->create([
            'type' => Transaction::TYPE_REFUND
        ]);

    }

    public function testSuccess(): void
    {
        $result = $this->put(route($this->routeName), [
            'uid' => $this->transaction->uid,
        ]);

        $result->seeStatusCode(Response::HTTP_INTERNAL_SERVER_ERROR);

        $this->seeInDatabase('transactions', [
            'uid' => $this->transaction->uid,
            'type' => $this->transaction->type
        ]);
    }
}
