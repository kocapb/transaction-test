<?php

namespace Features\Transaction\Cancel;

use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Response;

class ValidateParamTest extends Common
{
    public function testSuccess(): void
    {
        $this->put(route($this->routeName))
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJson()->response->assertJsonStructure([
                'uid',
            ]);

        $this->put(route($this->routeName), [
                'uid' => Str::random(),
            ])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJson()->response->assertJsonStructure([
                'uid',
            ]);

        $this->put(route($this->routeName), [
                'uid' => random_int(1, 1000)
            ])
            ->seeStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->seeJson()->response->assertJsonStructure([
                'uid',
            ]);
    }

    public function testNotFound(): void
    {
        $uid = Str::uuid()->toString();
        $this->put( route($this->routeName), [
                'uid' => $uid,
            ])
            ->seeStatusCode(Response::HTTP_NOT_FOUND);
    }
}
