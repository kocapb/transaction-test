<?php

namespace Features\Transaction\Cancel;

use TestCase;

abstract class Common extends TestCase
{
    /** @var string */
    protected $routeName = 'transaction.cancel';
}

