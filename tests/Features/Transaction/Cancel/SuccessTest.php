<?php

namespace Features\Transaction\Cancel;

use App\Models\Profile;
use App\Models\Transaction;

class SuccessTest extends Common
{
    /** @var Transaction */
    protected $transaction;
    /** @var Profile */
    protected $profile;

    public function setUp(): void
    {
        parent::setUp();

        $this->profile = Profile::factory()->create();
        $this->transaction = Transaction::factory()->create([
            'user_id' => $this->profile->user_id
        ]);

    }

    public function testSuccess(): void
    {
        $result = $this->put(route($this->routeName), [
            'uid' => $this->transaction->uid,
        ]);

        $result->response->assertOk();

        $this->seeInDatabase('transactions', [
            'uid' => $this->transaction->uid,
            'type' => Transaction::TYPE_REFUND,
        ]);
    }
}
