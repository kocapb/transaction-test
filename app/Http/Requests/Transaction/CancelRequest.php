<?php

namespace App\Http\Requests\Transaction;

use App\Http\Requests\BaseRequest;

/**
 * Class CancelRequest
 * @package App\Http\Requests\Transaction
 *
 * @OA\RequestBody(
 *     request="transaction_cancel_request"
 *     description="Отмена транзакции",
 *     required=true
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(property="uid",type="string",),
 *     )
 * ),
 *
 * @property string $uid
 */
class CancelRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'uid' => 'required|uuid',
        ];
    }
}
