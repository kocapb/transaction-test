<?php

namespace App\Http\Requests\Transaction;

use App\Http\Requests\BaseRequest;
use App\Models\Transaction;
use App\Rule\Money;
use Illuminate\Validation\Rule;

/**
 *
 * Class AddRequest
 * @package App\Http\Requests\Transaction
 *
 * @OA\RequestBody(
 *     request="transaction_add_request"
 *     description="Добавление транзакции",
 *     required=true
 *     @OA\JsonContent(
 *         type="object",
 *         @OA\Property(property="user_id",type="integer",),
 *         @OA\Property(property="value",type="integer",),
 *         @OA\Property(property="description",type="string",),
 *         @OA\Property(property="type",type="integer"),
 *     )
 * ),
 *
 * @property int $user_id
 * @property string $value
 * @property string $description
 * @property string $type
 */
class AddRequest extends BaseRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => ['required', 'integer', Rule::exists('profiles')],
            'value' => ['required', new Money()],
            'description' => 'required|string|min:3|max:1000',
            'type' => ['integer','required', Rule::in(Transaction::getAddTypes())]
        ];
    }
}
