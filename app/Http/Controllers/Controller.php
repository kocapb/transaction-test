<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\JsonResponse;

class Controller extends BaseController
{
    /** @var string */
    public const SUCCESS_KEY = 'success';
    /** @var string  */
    public const DATA_KEY = 'data';
    /** @var string  */
    public const ERROR_KEY = 'error';

    /**
     * @param array $data
     *
     * @return JsonResponse
     */
    public function success(array $data = []): JsonResponse
    {
        return response()->json([
            static::SUCCESS_KEY => true,
            static::DATA_KEY => $data,
        ], Response::HTTP_OK);
    }

    /**
     * @param array $data
     * @param int $code
     *
     * @return JsonResponse
     */
    public function error(array $data = [], int $code = Response::HTTP_INTERNAL_SERVER_ERROR): JsonResponse
    {
        return response()->json([
            static::SUCCESS_KEY => false,
            static::ERROR_KEY => $data
        ], $code);
    }
}
