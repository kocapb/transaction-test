<?php

namespace App\Http\Controllers\Transaction;

use App\Exceptions\CancelException;
use App\Http\Controllers\Controller;
use App\Http\Requests\Transaction\CancelRequest;
use App\Jobs\Profile\Balance;
use App\Jobs\Transaction\CancelJob;
use App\Models\Transaction;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Queue;
use Illuminate\Http\JsonResponse;

/**
 * Class CancelController
 * @package App\Http\Controllers\Transaction
 *
 * @OA\Schema(
 *     schema="transaction_cancel_model",
 *     type="object",
 *     description="Шаблон ответа модели отмены транзакции",
 *     @OA\Property(property="message",type="string",),
 * ),
 * @OA\Response(
 *     response="transaction_cancel_response",
 *     description="Ответ на отмену транзации",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(
 *             type="object",
 *             @OA\Property(property="success", type="string", example="true"),
 *             @OA\Property(property="data", type="array", @OA\Items(ref="#/components/schemas/transaction_cancel_model")),
 *         )
 *     )
 * ),
 */
class CancelController extends Controller
{
    /**
     * @OA\Put(
     *     path="/transaction/cancel",
     *     summary="Отмена транзакции",
     *     @OA\RequestBody(ref="#/components/requestBodies/transaction_cancel_request",),
     *     @OA\Response(response=200, ref="#/components/responses/transaction_cancel_response"),
     *     @OA\Response(response=404, ref="#/components/responses/404"),
     * )
     *
     * @param CancelRequest $request
     *
     * @return JsonResponse
     */
    public function __invoke(CancelRequest $request): JsonResponse
    {
        $transaction = $this->getTransaction($request->uid);

        if ($transaction->cantRefund()) {
            throw new CancelException('Транзакция уже отменена');
        }

        $cancelJob = new CancelJob($transaction);

        $this->dispatch(
            $cancelJob->chain([
                new Balance($transaction->uid)
            ])
        );

        return $this->success(['message' => 'Транзакция отправлена на отмену']);
    }

    /**
     * @param string $uid
     * @return Transaction|Model
     */
    private function getTransaction(string $uid): Transaction
    {
        return Transaction::query()->where(['uid' => $uid])->firstOrFail();
    }
}
