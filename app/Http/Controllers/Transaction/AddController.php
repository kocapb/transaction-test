<?php

namespace App\Http\Controllers\Transaction;

use App\DTO\Transaction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Transaction\AddRequest;
use App\Jobs\Profile\Balance;
use App\Jobs\Transaction\AddJob;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Str;

/**
 * Class AddController
 * @package App\Http\Controllers\Transaction
 *
 * @OA\Schema(
 *     schema="transaction_add_model",
 *     type="object",
 *     description="Шаблон ответа модели добавления транзакции",
 *     @OA\Property(property="message",type="string",),
 *     @OA\Property(property="uid",type="string",),
 * ),
 * @OA\Response(
 *     response="transaction_add_response",
 *     description="Ответ на добавление транзации",
 *     @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(
 *             type="object",
 *             @OA\Property(property="success", type="string", example="true"),
 *             @OA\Property(property="data",type="array", @OA\Items(ref="#/components/schemas/transaction_add_model")),
 *         )
 *     )
 * ),
 */
class AddController extends Controller
{
    /**
     * @OA\Post(
     *     path="/transaction/add",
     *     summary="Добавление транзакции",
     *     @OA\RequestBody(ref="#/components/requestBodies/transaction_add_request",),
     *     @OA\Response(response=200, ref="#/components/responses/transaction_add_response"),
     * )
     *
     * @param AddRequest $request
     *
     * @return JsonResponse
     */
    public function __invoke(AddRequest $request): JsonResponse
    {
        $uid = Str::uuid();
        $dto = Transaction::make(array_merge($request->validated(), ['uid' => $uid]));

        $addJob = new AddJob($dto);

        $this->dispatch(
            $addJob->chain([
                new Balance($uid)
            ])
        );

        return $this->success([
            'message' => 'Транзакция добавлена в очередь',
            'uid' => $uid,
        ]);
    }

    /**
     * @param int $id
     * @return User|Model
     */
    protected function getUser(int $id): User
    {
        return User::query()->find($id);
    }
}
