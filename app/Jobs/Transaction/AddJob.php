<?php

namespace App\Jobs\Transaction;

use App\Jobs\Job;
use App\Models\Transaction;
use App\DTO\Transaction as TransactionDTO;

class AddJob extends Job
{
    /** @var TransactionDTO */
    protected $transactionDTO;

    /**
     * AddJob constructor.
     * @param TransactionDTO $transaction
     */
    public function __construct(TransactionDTO $transaction)
    {
        $this->transactionDTO = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $model = new Transaction();
        $model->fill($this->transactionDTO->all());
        $model->save();
    }
}
