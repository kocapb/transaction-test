<?php


namespace App\Jobs\Transaction;


use App\Jobs\Job;
use App\Models\Transaction;

class CancelJob extends Job
{
    /** @var Transaction */
    protected $transaction;

    /**
     * CancelJob constructor.
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(): void
    {
        $this->transaction->type = Transaction::TYPE_REFUND;
        $this->transaction->save();
    }
}
