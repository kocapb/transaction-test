<?php

namespace App\Jobs\Profile;

use App\Exceptions\BalanceException;
use App\Jobs\Job;
use App\Models\Profile;
use App\Models\Transaction;
use Illuminate\Database\Eloquent\Model;

class Balance extends Job
{
    /** @var string */
    protected $uid;

    /**
     * Balance constructor.
     * @param string $uid
     */
    public function __construct(string $uid)
    {
        $this->uid = $uid;
    }

    public function handle(): void
    {
        $transaction = $this->getTransaction();
        $profile = $this->getProfileByUserId($transaction->user_id);

        switch ($transaction->type) {
            case Transaction::TYPE_DEBIT:
                if ($this->cantDebitBalance($profile->balance, $transaction->value)) {
                    throw new BalanceException('Списание не возможно, не хватает средств');
                }
            case Transaction::TYPE_REFILL:
                $profile->balance += $transaction->value;
                break;
            case Transaction::TYPE_REFUND:
                $profile->balance -= $transaction->value;
                break;
        }

        $profile->save();
    }

    /**
     * @return Transaction|Model
     */
    protected function getTransaction(): Transaction
    {
        return Transaction::query()->where('uid', $this->uid)->firstOrFail();
    }

    /**
     * @param int $userId
     *
     * @return Profile|Model
     */
    protected function getProfileByUserId(int $userId): Profile
    {
        return Profile::query()->where('user_id', $userId)->firstOrFail();
    }

    /**
     * @param float $balance
     * @param float $value
     *
     * @return bool
     */
    private function cantDebitBalance(float $balance, float $value): bool
    {
        return ($balance + $value) < 0;
    }
}
