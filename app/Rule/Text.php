<?php


namespace App\Rule;


use Illuminate\Contracts\Validation\Rule as Rule;

class Text implements Rule
{
    /**
     * @param string $attribute
     * @param mixed $value
     *
     * @return bool
     */
    public function passes($attribute, $value): bool
    {
        return preg_match('/^[\w-_.]*$/', $value);
    }

    /**
     * @return string
     */
    public function message(): string
    {
        return  'The :attribute must be of type text.';
    }
}
