<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Profile
 * @package App\Models
 *
 * @property int $user_id
 * @property float $balance
 */
class Profile extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'balance',
    ];

    /**
     * @param string $value
     */
    public function setBalanceAttribute(string $value): void
    {
        $this->attributes['balance'] = floatval($value);
    }
}
