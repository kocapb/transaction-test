<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

/**
 * Class BaseModel
 * @package App\Models
 *
 * @property int $id
 */
class BaseModel extends Model
{

}
