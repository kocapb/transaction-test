<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Transaction
 * @package App\Models
 *
 * @property int $user_id
 * @property float $value
 * @property string $text
 * @property int $type
 * @property string $uid
 */
class Transaction extends Model
{
    /** @const пополнение счета */
    public const TYPE_REFILL = 1;
    /** @const списание со счета */
    public const TYPE_DEBIT = 2;
    /** @const Возвращаемая транзакция */
    public const TYPE_REFUND = 3;

    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'type', 'value', 'description',  'uid',
    ];

    /**
     * @return array
     */
    public static function getAddTypes(): array
    {
        return [
            self::TYPE_REFILL,
            self::TYPE_DEBIT,
        ];
    }

    /**
     * @param array $options
     * @return bool
     */
    public function save(array $options = []): bool
    {
        if (self::TYPE_DEBIT === (int) $this->type) {

            $this->value = -1 * $this->value;
        }

        return parent::save($options);
    }

    public function cantRefund(): bool
    {
        return self::TYPE_REFUND === $this->type;
    }

    /**
     * @param string $value
     */
    public function setValueAttribute(string $value): void
    {
        $this->attributes['value'] = round(floatval($value), 2);
    }

}
