<?php


namespace App\DTO;

use ReflectionClass;
use ReflectionException;

abstract class BaseDTO
{
    /** @var array */
    protected $attributes;

    /**
     * BaseDTO constructor.
     * @param array $args
     *
     * @throws ReflectionException
     */
    public function __construct(array $args)
    {
        $reflection = new ReflectionClass(get_class($this));

        if ($reflection->getProperties()) {
            foreach ($reflection->getProperties() as $property) {
                $prop_name = $property->getName();

                if (isset($args[$prop_name])) {
                    $this->attributes[$prop_name] = $args[$prop_name];
                    $this->$prop_name = $args[$prop_name];
                }
            }
        }
    }

    /**
     * @param array $args
     * @return static
     *
     * @throws ReflectionException
     */
    public static function make(array $args): self
    {
        return new static($args);
    }

    public function all(): array
    {
        return $this->attributes;
    }
}
