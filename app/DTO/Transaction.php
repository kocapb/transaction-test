<?php


namespace App\DTO;

/**
 * Class Transaction
 * @package App\DTO
 *
 * @property int $user_id
 * @property string $value
 * @property string $description
 * @property int $type
 * @property string $uid
 */
class Transaction extends BaseDTO
{
    /** @var int */
    public $user_id;
    /** @var int */
    public $type;
    /** @var string */
    public $value;
    /** @var string */
    public $description;
    /** @var string */
    public $uid;
}
