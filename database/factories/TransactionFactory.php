<?php

namespace Database\Factories;

use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Str;

/**
 * Class TransactionFactory
 * @package Database\Factories
 */
class TransactionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Transaction::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        $value = random_int(-1000, 1000);

        return [
            'created_at' => Carbon::now()->subMinutes(random_int(0, 10000)),
            'user_id' => $this->createUser()->id,
            'value' => $value,
            'type' => $value < 0 ? Transaction::TYPE_DEBIT : Transaction::TYPE_REFILL,
            'description' => $this->getDescription($value),
            'uid' => Str::uuid()->toString(),
        ];
    }

    /**
     * @param $value
     * @return string
     */
    protected function getDescription($value) : string
    {
        $text = $value < 0 ? 'Списание' : 'Пополнение';
        $text .= ' на сумму ' . $value . ' '  . Lang::choice('рубль|рубля|рублей', $value, [], 'ru');

        return $text;
    }

    /**
     * @return User|Model
     */
    protected function createUser(): User
    {
        return User::factory()->create();
    }
}
