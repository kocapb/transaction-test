<?php

/**
 * @OA\Response(
 *      response=404,
 *      description="Model not found",
 *      @OA\MediaType(
 *         mediaType="application/json",
 *         @OA\Schema(example={"status": "success","data": "Model not found"})
 *     )
 * ),
 */
